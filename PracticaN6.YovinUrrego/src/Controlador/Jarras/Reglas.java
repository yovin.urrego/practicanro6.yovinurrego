package Controlador.Jarras;

import Controlador.Exceptions.PosicionException;
import Controlador.TDA_Lista.ListaEnlazada;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hilar_c9usj1g
 */
public class Reglas {

    public static ListaEnlazada<NodoJarras> reglas(Jarra jg, Jarra jp) {
        Integer x = jg.getCapacidadActual();
        Integer y = jp.getCapacidadActual();
        ListaEnlazada<NodoJarras> reglas = new ListaEnlazada<>();

        try {
            
            //Llenar toda la jarra grande
            if (x < jg.getCapacidad()) {
                reglas.insertar(new NodoJarras(jg.getCapacidad(), y), posicion(reglas));
            }

            //Llenar jarra pequeña
            if (y < jp.getCapacidad()) {
                reglas.insertar(new NodoJarras(x, jp.getCapacidad()), posicion(reglas));
            }

            // Vaciar jarra grande
            if (x > 0) {
                reglas.insertar(new NodoJarras(x, 0), posicion(reglas));
            }

            //Vaciar el contenido de la jarra grande a la pequeña hasta llenarla
            if (((x + y) >= jp.getCapacidad()) && (y < jp.getCapacidad() && x > 0)) {
                reglas.insertar(new NodoJarras(x - (jp.getCapacidad() - y), jp.getCapacidad()), posicion(reglas));
            }

            //Vaciar el contenido de la jarra pequeña a la grande hasta llenarla
            if (((x + y) >= jg.getCapacidad()) && (y < jg.getCapacidad() && y > 0)) {
                reglas.insertar(new NodoJarras(jg.getCapacidad(), y - (jg.getCapacidad() - x)), posicion(reglas));
            }

            //Vaciar todo el contenido de la jarra grande a la jarra pequeña
            //Vaciar todo el contenido de la jarra pequeña a la grande
        } catch (PosicionException ex) {
            System.out.println("ERROR EN REGLAS: " + ex);
        }

        return reglas;
    }

    public static Integer posicion(ListaEnlazada<NodoJarras> lista) {
        return (lista.getSize() > 0) ? lista.getSize() - 1 : 0;
    }

}
