package Controlador.Jarras;

/**
 *
 * @author hilar_c9usj1g
 */
public class Jarra {
    
    private Integer capacidad;
    private Integer capacidadActual;

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public Integer getCapacidadActual() {
        return capacidadActual;
    }

    public void setCapacidadActual(Integer capacidadActual) {
        this.capacidadActual = capacidadActual;
    }

    @Override
    public String toString() {
        return "Jarra(" + "Capacidad=" +  capacidad+ "L)= " + capacidadActual;
    }
    
    
    
}
