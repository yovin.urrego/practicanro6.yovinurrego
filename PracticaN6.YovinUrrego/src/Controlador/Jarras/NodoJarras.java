package Controlador.Jarras;

import Controlador.TDA_Lista.ListaEnlazada;

/**
 *
 * @author hilar_c9usj1g
 */
public class NodoJarras {
    
    private Jarra jarraG;
    private Jarra jarraP;
    private NodoJarras padre;
    private ListaEnlazada<NodoJarras> sucesores;

    public NodoJarras() {
        jarraG = new Jarra();
        jarraP = new Jarra();
        jarraG.setCapacidad(4);
        jarraP.setCapacidad(3);
        jarraG.setCapacidadActual(0);
        jarraP.setCapacidadActual(0);
    }
    
    public NodoJarras(Integer x, Integer y) {
        this();
        jarraG.setCapacidadActual(x);
        jarraP.setCapacidadActual(y);
    }
    
    public static Boolean comparar(NodoJarras i, NodoJarras f){
        if (i != null && f != null) {
            if (i.getJarraG().getCapacidadActual().intValue() == f.getJarraG().getCapacidadActual().intValue()
                && i.getJarraP().getCapacidadActual().intValue() == f.getJarraP().getCapacidadActual().intValue()){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return jarraG.toString() + "," + jarraP.toString();
    }
    
    
    

    public Jarra getJarraG() {
        return jarraG;
    }

    public void setJarraG(Jarra jarraG) {
        this.jarraG = jarraG;
    }

    public Jarra getJarraP() {
        return jarraP;
    }

    public void setJarraP(Jarra jarraP) {
        this.jarraP = jarraP;
    }

    public NodoJarras getPadre() {
        return padre;
    }

    public void setPadre(NodoJarras padre) {
        this.padre = padre;
    }

    public ListaEnlazada<NodoJarras> getSucesores() {
        return sucesores;
    }

    public void setSucesores(ListaEnlazada<NodoJarras> sucesores) {
        this.sucesores = sucesores;
    }
    
    
    
}
