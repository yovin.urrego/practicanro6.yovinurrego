package Controlador.TDA_Arboles;

import Controlador.Exceptions.PosicionException;
import Controlador.TDA_Lista.ListaEnlazada;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hilar_c9usj1g
 */
public class Arbol {

    private NodoArbol raiz;
    private Integer tamanio;
    private Integer altura;
    private ListaEnlazada<ListaEnlazada<NodoArbol>> niveles;
    private ListaEnlazada<NodoArbol> orden;

    public Arbol() {
        raiz = null;
        altura = 0;
        tamanio = 0;
        niveles = new ListaEnlazada<>();
        orden = new ListaEnlazada<>();
    }

    public Boolean insertar(Integer valor) {
        if (raiz == null) {
            raiz = new NodoArbol(valor);
            tamanio++;
            this.altura = calcularAltura(raiz);
            niveles = new ListaEnlazada<>();
            for (int i = 0; i < this.altura; i++) {
                niveles.insertarCabecera(new ListaEnlazada<>());
            }
            calcularNiveles(raiz, 0);
            try {
                niveles.eliminarDato(niveles.getSize() - 1);
            } catch (PosicionException ex) {
                System.out.println("ERROR EN ELIMINAR: " + ex);
            }
            return true;

        } else {
            NodoArbol nuevo = new NodoArbol(valor);
            NodoArbol actual = raiz;
            NodoArbol padre;
            while (true) {
                padre = actual;
                if (valor.intValue() == actual.getValor()) {
                    return false;
                } else if (valor.intValue() < actual.getValor().intValue()) {
                    actual = actual.getIzquierda();
                    if (actual == null) {
                        nuevo.setPadre(padre);
                        padre.setIzquierda(nuevo);
                        tamanio++;
                        this.altura = calcularAltura(raiz);
                        niveles = new ListaEnlazada<>();
                        for (int i = 0; i < this.altura; i++) {
                            niveles.insertarCabecera(new ListaEnlazada<>());
                        }
                        calcularNiveles(raiz, 0);
                        try {
                            niveles.eliminarDato(niveles.getSize() - 1);
                        } catch (PosicionException ex) {
                            System.out.println("ERROR EN ELIMINAR: " + ex);
                        }
                        return true;
                    }

                } else {
                    actual = actual.getDerecha();
                    if (actual == null) {
                        nuevo.setPadre(padre);
                        padre.setDerecha(nuevo);
                        tamanio++;
                        this.altura = calcularAltura(raiz);
                        niveles = new ListaEnlazada<>();
                        for (int i = 0; i < this.altura; i++) {
                            niveles.insertarCabecera(new ListaEnlazada<>());
                        }
                        calcularNiveles(raiz, 0);
                        try {
                            niveles.eliminarDato(niveles.getSize() - 1);
                        } catch (PosicionException ex) {
                            System.out.println("ERROR EN ELIMINAR: " + ex);
                        }
                        return true;
                    }
                }
            }

        }
    }

    public void calcularNiveles(NodoArbol arbol, Integer nivel) {
        try {
            if (arbol != null) {
                int pos = (niveles.obtenerDato(nivel).getSize() > 0) ? niveles.obtenerDato(nivel).getSize() - 1 : 0;
                niveles.obtenerDato(nivel).insertar(arbol, pos);
                nivel++;
                calcularNiveles(arbol.getIzquierda(), nivel);
                calcularNiveles(arbol.getDerecha(), nivel);
            } else if (nivel != getAltura()) {
                int pos = (niveles.obtenerDato(nivel).getSize() > 0) ? niveles.obtenerDato(nivel).getSize() - 1 : 0;
                niveles.obtenerDato(nivel).insertar(null, pos);
                nivel++;
                calcularNiveles(null, nivel);
                calcularNiveles(null, nivel);
            }

        } catch (Exception e) {
            System.out.println("ERROR EN NIVELES: " + e);
        }
    }

    public Integer calcularAltura(NodoArbol arbol) {
        if (arbol == null) {
            return 0;
        } else {
            Integer izquierda = calcularAltura(arbol.getIzquierda());
            Integer derecha = calcularAltura(arbol.getDerecha());
            if (izquierda > derecha) {
                return izquierda + 1;
            } else {
                return (derecha + 1);
            }
        }
    }

    public NodoArbol getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoArbol raiz) {
        this.raiz = raiz;
    }

    public Integer getTamanio() {
        return tamanio;
    }

    public void setTamanio(Integer tamanio) {
        this.tamanio = tamanio;
    }

    public Integer getAltura() {
        return altura;
    }

    public void setAltura(Integer altura) {
        this.altura = altura;
    }

    public ListaEnlazada<ListaEnlazada<NodoArbol>> getNiveles() {
        return niveles;
    }

    public void setNiveles(ListaEnlazada<ListaEnlazada<NodoArbol>> niveles) {
        this.niveles = niveles;
    }

    public ListaEnlazada<NodoArbol> getOrden() {
        return orden;
    }

    public void setOrden(ListaEnlazada<NodoArbol> orden) {
        this.orden = orden;
    }

    public void preOrden(NodoArbol arbol) throws PosicionException {
        if (arbol != null) {
            int pos = (orden.getSize() > 0) ? orden.getSize() - 1 : 0;
            orden.insertar(arbol, pos);
            preOrden(arbol.getIzquierda());
            preOrden(arbol.getDerecha());
        }
    }

    public ListaEnlazada<NodoArbol> preOrder() {
        orden = new ListaEnlazada<>();
        try {
            preOrden(raiz);
        } catch (PosicionException e) {
            System.out.println("ERROR EN PRE-ORDEN: " + e);
        }
        return orden;
    }

    public void posOrden(NodoArbol arbol) throws PosicionException {
        if (arbol != null) {
            preOrden(arbol.getIzquierda());
            preOrden(arbol.getDerecha());
            int pos = (orden.getSize() > 0) ? orden.getSize() - 1 : 0;
            orden.insertar(raiz, pos);
        }
    }

    public ListaEnlazada<NodoArbol> posOrder() {
        orden = new ListaEnlazada<>();
        try {
            posOrden(raiz);
        } catch (PosicionException e) {
            System.out.println("ERROR EN POS-ORDEN: " + e);
        }
        return orden;
    }

    public void inOrden(NodoArbol arbol) throws PosicionException {
        if (arbol != null) {
            preOrden(arbol.getIzquierda());
            int pos = (orden.getSize() > 0) ? orden.getSize() - 1 : 0;
            orden.insertar(raiz, pos);
            preOrden(arbol.getDerecha());
        }
    }

    public ListaEnlazada<NodoArbol> inOrder() {
        orden = new ListaEnlazada<>();
        try {
            inOrden(raiz);
        } catch (PosicionException e) {
            System.out.println("ERROR EN IN-ORDEN: " + e);
        }
        return orden;
    }

    public static void main(String[] args) {
        Arbol a = new Arbol();
        a.insertar(46);
        a.insertar(26);
        a.insertar(14);
        a.insertar(10);
        a.insertar(65);
        a.insertar(70);
        a.insertar(62);
        a.insertar(28);
        a.insertar(27);
        a.insertar(29);
        a.insertar(15);
        a.insertar(60);
        a.insertar(64);
        a.insertar(66);
        a.insertar(77);

        System.out.println("Nro de Nodos: " + a.getTamanio());
        System.out.println("Altura: " + a.getAltura());
        System.out.println("Niveles: " + a.getNiveles().getSize());
        System.out.println("**************************\nIMPRIMIR NIVELES");
        Integer pos = 50;
        for (int i = 0; i < a.getNiveles().getSize(); i++) {
            try {
                ListaEnlazada<NodoArbol> lista = a.getNiveles().obtenerDato(i);
                Integer posA = pos / lista.getSize();
                String cadena = a.posiciones(posA);
                Integer espacios = posA;
                for (int j = 0; j < lista.getSize(); j++) {
                    try {
                        if (lista.obtenerDato(j) != null) {
                            cadena = cadena.substring(0, cadena.length() - 2);
                            System.out.print(cadena + lista.obtenerDato(j).getValor());
                            espacios += posA;
                            if (i >= 2 && j >= 1) {
                                espacios -= posA;
                            }
                            cadena = a.posiciones(espacios);
                        } else {
                            System.out.print(cadena);
                        }
                    } catch (PosicionException ex) {
                        Logger.getLogger(Arbol.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                System.out.println();
            } catch (PosicionException ex) {
                Logger.getLogger(Arbol.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("PRE ORDEN");
        a.preOrder().imprimir();
        System.out.println("POS ORDEN");
        a.posOrder().imprimir();

        System.out.println("IN ORDEN");
        a.inOrder().imprimir();
    }

    public String posiciones(Integer aux) {
        StringBuilder cadena = new StringBuilder();
        for (int i = 0; i < aux; i++) {
            cadena.append(" ");
        }
        return cadena.toString();
    }
}
