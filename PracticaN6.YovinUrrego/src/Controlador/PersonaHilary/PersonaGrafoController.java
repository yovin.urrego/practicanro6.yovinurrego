package Controlador.PersonaHilary;

import Controlador.TDA_Grafos.GrafoEND;
import Modelo.Persona;
import Modelo.TipoPersona;
import Modelo.Ubicacion;

/**
 *
 * @author hilar_c9usj1g
 */
public class PersonaGrafoController {
    
    private GrafoEND<Persona> grafoEND;
    private Persona persona;


    public PersonaGrafoController(Integer nro_vertices) {
        grafoEND= new GrafoEND<>(nro_vertices, Persona.class);
        for(int i = 1; i <= nro_vertices; i++) {
            Persona p = new Persona();
            p.setId(i);
            p.setNombres("Persona "+i);
            p.setTipoPersona(TipoPersona.CLIENTE);
            Ubicacion u = new Ubicacion();
            u.setId(i);
            u.setLatitud(0.0);
            u.setLongitud(0.0);
            p.setUbicacion(u);
            grafoEND.etiquetarVertice(i, p);
        }
    }


    public PersonaGrafoController() {
    }

    public GrafoEND<Persona> getGrafoEND() {
        return grafoEND;
    }

    public void setGrafoEND(GrafoEND<Persona> grafoEND) {
        this.grafoEND = grafoEND;
    }

    public Persona getPersona() {
        if (persona==null) {
            persona = new Persona();           
        }
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    
    public Double calcularDistancia(Persona po, Persona pd) {
        // raiz cuadrara ((x1 - x2)elevado al cuadradro + (y1 - y2)elevado al cuadradro)
        Double dis = 0.0;
        Double x = po.getUbicacion().getLongitud() - pd.getUbicacion().getLongitud();
        Double y = po.getUbicacion().getLatitud() - pd.getUbicacion().getLatitud();
        dis = Math.sqrt((x*x) + (y*y));
        return dis;
    }

    
}
