package Controlador.TDA_Grafos;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Lista.ListaEnlazada;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author hilar_c9usj1g
 */
public class GrafoDE<E> extends GrafoD {

    protected Class<E> clazz;
    protected E etiquetas[];
    protected HashMap<E, Integer> dicVertices;

    public GrafoDE(Integer numV, Class clazz) {
        super(numV);
        this.clazz = clazz;
        etiquetas = (E[]) java.lang.reflect.Array.newInstance(this.clazz, numV + 1);
        dicVertices = new HashMap<>(numV);
    }

    public Object[] existeAristaE(E i, E j) throws Exception {
        return this.existeArista(obtenerCodigo(i), obtenerCodigo(j));
    }

    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
    }

    public void insertarAristaE(E i, E j) throws Exception {
        this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), Double.NaN);
    }

    public Integer obtenerCodigo(E etiqueta) throws Exception {
        Integer key = dicVertices.get(etiqueta);
        if (key != null) {
            return key;
        } else {
            return -1;
        }
    }

    public E obtenerEtiqueta(Integer codigo) throws Exception {

        return etiquetas[codigo];
    }

    public ListaEnlazada<Adyacencia> adyacentesDEE(E i) throws Exception {
        return adyacente(obtenerCodigo(i));
    }

    public void etiquetarVertice(Integer codigo, E etiqueta) {
        etiquetas[codigo] = etiqueta;
        dicVertices.put(etiqueta, codigo);
    }

    public Boolean modificar(E viejo, E nuevo) throws Exception {
        Integer pos = obtenerCodigo(viejo);
        etiquetas[pos] = nuevo;
        dicVertices.remove(viejo);
        dicVertices.put(nuevo, pos);
        return true;
    }

    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder();
        try {
            for (int i = 1; i <= numVertices(); i++) {
                grafo.append("VERTICE " + i + " --E-- " + obtenerEtiqueta(i).toString());
                try {
                    ListaEnlazada<Adyacencia> lista = adyacente(i);

                    for (int j = 0; j < lista.getSize(); j++) {
                        Adyacencia aux = lista.obtenerDato(j);
                        if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                            grafo.append(" --- VERTICE DESTINO " + aux.getDestino() + " --E-- " + obtenerEtiqueta(aux.getDestino()));
                        } else {
                            grafo.append(" --- VERTICE DESTINO " + aux.getDestino() + " --E-- " + obtenerEtiqueta(aux.getDestino()) + " --peso-- " + aux.getPeso());
                        }
                    }
                    grafo.append("\n");
                } catch (Exception e) {
                    System.out.println("Error en toString " + e);

                }
            }
        } catch (Exception e) {
            System.out.println("Error en toString" + e);
        }
        return grafo.toString();
    }

    public ListaEnlazada[] dijkStra(String inicio, String fin, boolean todosDatos) throws Exception {

        int codigoInicio = obtenerCodigo((E) inicio);
        int finx = obtenerCodigo((E) fin);
        boolean[] isVisited = new boolean[this.numVertices() + 1];
        double[] distance = new double[this.numVertices()];
        String[] path = new String[numVertices()];
        for (int i = 0; i < numVertices(); i++) {
            distance[i] = Double.POSITIVE_INFINITY;
            path[i] = "";
        }
//        System.out.println(Arrays.toString(isVisited));
//        //    System.out.println(numVertices());
//        System.out.println(Arrays.toString(distance));
        distance[codigoInicio - 1] = 0.0;
        // CO son las coordenadas necesarias para la iteración, headIndex es el vértice inicial de cada DIJKSTRA
        int CO;
        int headIndex = codigoInicio;
        //     System.out.println(codigoInicio + " " + finx);
        // Luego haz lo siguiente para cada vértice
        // 1. Establece este vértice en conocido, no te preocupes por la distancia y la ruta de este punto, porque ha sido diseñado antes
        // 2. Encuentra cada vértice adyacente de este vértice. Para un vértice desconocido, compare la distancia alcanzada a lo largo de este vértice con su distancia original, si es menor que la distancia original, actualice la distancia y actualice la ruta
        // 3. Después de establecer este vértice, use la función indexGet para encontrar el vértice con la distancia más pequeña entre los vértices desconocidos actuales, y utilícelo como el siguiente vértice para realizar el paso 2
        while (!isVisited[headIndex - 1]) {

//            System.out.println(headIndex);
            // CO es la primera CO que no ha sido visitada
            CO = getFirstCO(headIndex);
//            System.out.println(CO);
            while (isVisited[CO - 1]) {
                CO = getNextCO(headIndex, CO);
            }

            // Si el vértice headIndex no tiene vértices adyacentes que no hayan sido visitados, la coordenada del vértice se obtiene como n, lo que indica que es el último nodo desconocido, y solo necesita establecerse como conocido
            if (CO == numVertices() + 1) {
                isVisited[headIndex - 1] = true;
                //System.out.println("Coordinate not found ");
            } // Ejecuta el paso 2 para todos los vértices adyacentes a través de un bucle
            else {

                while (!isVisited[CO - 1] && CO < this.numVertices() + 1) {
                    isVisited[headIndex - 1] = true;
                    Object[] resultado = existeAristaE(obtenerEtiqueta(headIndex), obtenerEtiqueta(CO));
                    double currentDis = distance[headIndex - 1] + (double) resultado[1];
//                    System.out.println(pesoArista(headIndex+1, CO+1));
                    if (currentDis < distance[CO - 1]) {
                        distance[CO - 1] = currentDis;

                        path[CO - 1] = path[headIndex - 1] + " " + this.obtenerEtiqueta(headIndex);
                    }

                    CO = getNextCO(headIndex, CO);

                }
            }
//            System.out.println("HE " + headIndex);
//            System.out.println("CO " + CO);
            headIndex = indexGet(distance, isVisited);
//            System.out.println(Arrays.toString(distance));
//            System.out.println(Arrays.toString(isVisited));

        }
        for (int i = 0; i < this.numVertices(); i++) {
            path[i] = path[i] + " " + this.obtenerEtiqueta(i + 1);
        }

//        System.out.println("Iniciar nodo:" + this.obtenerEtiqueta(codigoInicio));
        if (todosDatos) {
            ListaEnlazada[] listaDAtos = new ListaEnlazada[numVertices()];
            for (int i = 0; i < this.numV; i++) {
                listaDAtos[i] = new ListaEnlazada<>();
            }
            for (int i = 0; i < numVertices(); i++) {
                listaDAtos[i].insertarCabecera(path[i]);
                listaDAtos[i].insertarCabecera(distance[i]);
                listaDAtos[i].insertarCabecera(this.obtenerEtiqueta(i + 1));
            }
            return listaDAtos;
        } else {
            ListaEnlazada[] listaDAtos = new ListaEnlazada[1];
            listaDAtos[0] = new ListaEnlazada<>();
            listaDAtos[0].insertarCabecera(path[finx - 1]);
            listaDAtos[0].insertarCabecera(distance[finx - 1]);
            listaDAtos[0].insertarCabecera(this.obtenerEtiqueta(finx));
            return listaDAtos;
        }
//        System.out.println(Arrays.toString(isVisited));
//        System.out.println(Arrays.toString(distance));
    }

    // Devuelve el siguiente vértice requerido a través de la matriz de distancia y la matriz de acceso dadas
    public int indexGet(double[] distance, boolean[] isVisited) {
        int j = 0;
        double mindis = Double.POSITIVE_INFINITY;
        for (int i = 0; i < distance.length; i++) {
            if (!isVisited[i]) {
                if (distance[i] < mindis) {
                    mindis = distance[i];
                    j = i;
                }
            }
        }
        return j + 1;
    }
    // Obtiene el primer vértice adyacente del vértice especificado

    public int getFirstCO(int index) throws VerticeException, Exception {
        for (int i = 1; i <= this.numVertices(); i++) {
//                System.out.println((String)obtenerEtiqueta(i )+ (String)obtenerEtiqueta(index));
            Object[] resultado = existeAristaE(obtenerEtiqueta(index), obtenerEtiqueta(i));

            //     System.out.println(i + " i---index " + index);
            if ((Double) resultado[1] > 0) {
                return i;
            }
        }
        return this.numVertices() + 1;
    }

    // Obtiene los vértices adyacentes secuenciales del vértice especificado
    public int getNextCO(int index, int firstCO) throws VerticeException, Exception {
        for (int i = firstCO + 1; i <= this.numVertices(); i++) {

            Object[] resultado = existeAristaE(obtenerEtiqueta(index), obtenerEtiqueta(i));
            //    System.out.println(i + " i---index" + index);
            if ((Double) resultado[1] > 0) {
                //     System.out.println(resultado[1]);
                return i;
            }
        }
        return this.numVertices() + 1;
    }

    public static void main(String[] args) throws Exception {
        GrafoDE garfo = new GrafoDE(5, String.class);
        garfo.generar();
    }

    public void generar() throws VerticeException, Exception {
        GrafoDE garfo = new GrafoDE(9, String.class);
        garfo.etiquetarVertice(1, "A");
        garfo.etiquetarVertice(2, "B");
        garfo.etiquetarVertice(3, "C");
        garfo.etiquetarVertice(4, "D");
        garfo.etiquetarVertice(5, "E");
        garfo.etiquetarVertice(6, "F");
        garfo.etiquetarVertice(7, "G");
        garfo.etiquetarVertice(8, "H");
        garfo.etiquetarVertice(9, "I");

        garfo.insertarAristaE("A", "B", 10.0);
        garfo.insertarAristaE("A", "C", 2.0);
        garfo.insertarAristaE("A", "D", 1.0);
        garfo.insertarAristaE("C", "E", 7.0);
        garfo.insertarAristaE("C", "D", 7.0);
        garfo.insertarAristaE("B", "E", 1.0);
        garfo.insertarAristaE("E", "D", 3.0);
        garfo.insertarAristaE("E", "F", 3.0);
        garfo.insertarAristaE("D", "G", 3.0);
        garfo.insertarAristaE("D", "H", 3.0);
        garfo.insertarAristaE("I", "H", 3.0);

        garfo.insertarAristaE("B", "A", 10.0);
        garfo.insertarAristaE("C", "A", 2.0);
        garfo.insertarAristaE("D", "A", 1.0);
        garfo.insertarAristaE("E", "C", 7.0);
        garfo.insertarAristaE("D", "C", 7.0);
        garfo.insertarAristaE("E", "B", 1.0);
        garfo.insertarAristaE("D", "E", 3.0);
        garfo.insertarAristaE("F", "E", 3.0);
        garfo.insertarAristaE("G", "D", 3.0);
        garfo.insertarAristaE("H", "D", 3.0);
        garfo.insertarAristaE("H", "I", 3.0);
//        
//          garfo.insertarAristaE("B", "A", 10.0);
//        garfo.insertarAristaE("C", "A", 2.0);
////        garfo.insertarAristaE("C", "B", 1.0);
//        garfo.insertarAristaE("E", "C", 7.0);
//        garfo.insertarAristaE("E", "B", 1.0);
//        garfo.insertarAristaE("D", "E", 3.0);
        //  garfo.comprobasF();
        //      garfo.dijkStra("A", "E", true);
        //   System.out.println(garfo.existeAristaE("E", "D")[1]);
 //  Integer[] grajo = garfo.toArrayDFS();
 Integer[] grajo = garfo.BPA(1);
        for (int i = 0; i < grajo.length; i++) {
            System.out.println(grajo[i]);
        }
//        for (int i = 1; i <= garfo.numV; i++) {
//            for (int j = 1; j <= garfo.numA; j++) {
//                Object[] resultado = existeArista(i, j);
//                //    System.out.println(i + " i---index" + index);
//                if ((Double) resultado[1] > 0) {
//                    System.out.println(resultado[1]);
//                }
//            }
//            System.out.println("-----");
//        }
    }

}
