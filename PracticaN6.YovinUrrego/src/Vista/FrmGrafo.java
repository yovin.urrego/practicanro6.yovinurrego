package Vista;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Grafos.GrafoD;
import Controlador.TDA_Grafos.GrafoDE;
import Controlador.TDA_Grafos.GrafoEND;
import Controlador.TDA_Grafos.GrafoND;
import Controlador.TDA_Lista.ListaEnlazada;
import Vista.Tablas.ModeloTablaGrafo;
import Vista.Tablas.ModeloTablaGrafoEtiquetado;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author hilar_c9usj1g
 */
public class FrmGrafo extends javax.swing.JFrame {

    private GrafoDE gd = new GrafoDE(0, String.class);
    private ModeloTablaGrafo tgd = new ModeloTablaGrafo();
    private boolean isDE = true;

    public FrmGrafo() {
        initComponents();
        jPanel4.setVisible(false);
        jPanel3.setVisible(false);
        jPanel2.setVisible(false);
        jPanel7.setVisible(false);
        jPanel8.setVisible(false);
    }

    private void generarGrafo() throws Exception {
        if (!String.valueOf(this.nombreVertice.getText()).equalsIgnoreCase("")
         && -1 == gd.obtenerCodigo(String.valueOf(this.nombreVertice.getText()))) {
            GrafoDE aux = gd;
            if (opcGD.isSelected()) {
                if (!isDE) {
                    aux = new GrafoDE(0, String.class);
                    JOptionPane.showMessageDialog(null, "    Grafo borrado");
                }
                isDE = true;
                this.gd = new GrafoDE(aux.numVertices() + 1, String.class);
                for (int i = 1; i <= aux.numVertices(); i++) {
                    gd.etiquetarVertice(i, aux.obtenerEtiqueta(i));
                }
                for (int i = 1; i <= aux.numVertices(); i++) {
                    for (int j = 0; j < aux.adyacente(i).getSize(); j++) {
                        gd.insertarAristaE(aux.obtenerEtiqueta(i), 
                         aux.obtenerEtiqueta(aux.adyacente(i).obtenerDato(j).getDestino()),
                       (Double) aux.existeAristaE(aux.obtenerEtiqueta(i), 
                        aux.obtenerEtiqueta(aux.adyacente(i).obtenerDato(j).getDestino()))[1]);
                    }
                }
                gd.etiquetarVertice(gd.numVertices(), String.valueOf(this.nombreVertice.getText()));

            } else {
                if (opcGND.isSelected()) {
                    if (isDE) {
                        aux = new GrafoDE(0, String.class);
                        JOptionPane.showMessageDialog(null, "    Grafo borrado");
                    }
                    isDE = false;
                    this.gd = new GrafoEND(aux.numVertices() + 1, String.class);
                    for (int i = 1; i <= aux.numVertices(); i++) {
                        gd.etiquetarVertice(i, aux.obtenerEtiqueta(i));

                    }
                    for (int i = 1; i <= aux.numVertices(); i++) {
                        for (int j = 0; j < aux.adyacente(i).getSize(); j++) {
                            gd.insertarAristaE(aux.obtenerEtiqueta(i), 
                           aux.obtenerEtiqueta(aux.adyacente(i).obtenerDato(j).getDestino()), 
                          (Double) aux.existeAristaE(aux.obtenerEtiqueta(i), 
                           aux.obtenerEtiqueta(aux.adyacente(i).obtenerDato(j).getDestino()))[1]);
                        }
                    }
                    gd.etiquetarVertice(gd.numVertices(), String.valueOf(this.nombreVertice.getText()));

                } else {
                    JOptionPane.showMessageDialog(null, "Seleccionar un tipo de grafo", "ERROR",
                   JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Error con nombre", "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        cbxFin.removeAllItems();
        cbxOrigen1.removeAllItems();
        cbxDestino.removeAllItems();
        cbxinicio.removeAllItems();
        for (int i = 1; i <= gd.numVertices(); i++) {
            cbxFin.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cbxOrigen1.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cbxDestino.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cbxinicio.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cargarTabla();
            jPanel3.setVisible(true);
            jPanel4.setVisible(true);
            this.nombreVertice.setText(null);
        }
        System.out.println(gd.numAristas()+ "  "+ gd.numVertices());
    }

    public DefaultTableModel getTableListaDis(Boolean todosDatos) throws Exception {
        ListaEnlazada listaDis[] = gd.dijkStra(String.valueOf(cbxinicio.getSelectedItem().toString()), String.valueOf(cbxFin.getSelectedItem().toString()), todosDatos);
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Vertice llegada");
        model.addColumn("Peso");
        model.addColumn("Recorrido");
        String datos[] = new String[3];
        for (int i = 0; i < listaDis.length; i++) {
            datos[0] = String.valueOf(listaDis[i].obtenerDato(0));
            datos[1] = String.valueOf(listaDis[i].obtenerDato(1));
            datos[2] = String.valueOf(listaDis[i].obtenerDato(2));
            model.addRow(datos);
        }
        return model;
    }

    private void cargarTabla() throws Exception {
        tgd.setGrafoED(gd);
        tblTabla.setModel(tgd);
        tgd.fireTableStructureChanged();
        tblTabla.updateUI();
    }

    private void adyacencias() throws Exception {
        String i = String.valueOf(cbxOrigen1.getSelectedItem().toString());
        String j = String.valueOf(cbxDestino.getSelectedItem().toString());

        if (i.equalsIgnoreCase(j)) {
            JOptionPane.showMessageDialog(null, "Vertices iguales", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                gd.insertarAristaE(i, j, Double.parseDouble(this.peso.getText()));
                cargarTabla();
            } catch (VerticeException ex) {
                System.out.println("ERROR EN INSERTAR ADYACENCIA: " + ex);
                JOptionPane.showMessageDialog(null, "No se puede insertar", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
        this.peso.setText(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TipoGrafo = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbxDestino = new javax.swing.JComboBox<>();
        peso = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cbxOrigen1 = new javax.swing.JComboBox<>();
        btnOk1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnMostrar = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        opcGND = new javax.swing.JRadioButton();
        opcGD = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        btnModificar = new javax.swing.JButton();
        nombreVertice = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cbxFin = new javax.swing.JComboBox<>();
        btnOk = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        cbxinicio = new javax.swing.JComboBox<>();
        btnOk2 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaDis = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        textBPA = new javax.swing.JLabel();
        textPraBBP = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Vertice origen");

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Vertice destino");

        cbxDestino.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));

        jLabel6.setText("Peso;");

        cbxOrigen1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));

        btnOk1.setBackground(new java.awt.Color(51, 51, 51));
        btnOk1.setForeground(new java.awt.Color(255, 255, 255));
        btnOk1.setText("Conectar");
        btnOk1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOk1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbxOrigen1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbxDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnOk1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(10, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(cbxDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxOrigen1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addContainerGap(10, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnOk1)
                        .addContainerGap())))
        );

        jPanel1.add(jPanel3);
        jPanel3.setBounds(340, 80, 290, 80);

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel4.setLayout(null);

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel4.add(jScrollPane1);
        jScrollPane1.setBounds(20, 30, 560, 170);

        btnMostrar.setBackground(new java.awt.Color(51, 51, 51));
        btnMostrar.setForeground(new java.awt.Color(255, 255, 255));
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        jPanel4.add(btnMostrar);
        btnMostrar.setBounds(500, 220, 75, 24);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(20, 170, 610, 260);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel6.setForeground(new java.awt.Color(206, 170, 152));

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Tipo de grafo");

        TipoGrafo.add(opcGND);
        opcGND.setForeground(new java.awt.Color(0, 0, 0));
        opcGND.setText("Grafo no dirigido");
        opcGND.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcGNDActionPerformed(evt);
            }
        });

        TipoGrafo.add(opcGD);
        opcGD.setForeground(new java.awt.Color(0, 0, 0));
        opcGD.setText("Grafo dirigido");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel4)
                .addGap(50, 50, 50)
                .addComponent(opcGD, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(opcGND, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(208, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(opcGND)
                    .addComponent(opcGD))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel6);
        jPanel6.setBounds(20, 30, 610, 40);

        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnModificar.setText("Agregar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        jLabel7.setText("Nombre:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(nombreVertice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(68, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(164, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(nombreVertice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnModificar)
                .addContainerGap(11, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(27, 27, 27)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(39, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel5);
        jPanel5.setBounds(20, 70, 310, 90);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setText("Algoritmo dijkStra");

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Vertice inicio");

        cbxFin.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));

        btnOk.setBackground(new java.awt.Color(51, 51, 51));
        btnOk.setForeground(new java.awt.Color(255, 255, 255));
        btnOk.setText("Obtener tabla");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Vertice destino");

        cbxinicio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));

        btnOk2.setBackground(new java.awt.Color(51, 51, 51));
        btnOk2.setForeground(new java.awt.Color(255, 255, 255));
        btnOk2.setText("Obtener camino");
        btnOk2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOk2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbxFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbxinicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(40, 40, 40)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnOk2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(109, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(btnOk)
                    .addComponent(cbxinicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(cbxFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOk2))
                .addContainerGap(66, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(660, 30, 390, 160);

        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tablaDis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaDis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaDisMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaDis);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel7);
        jPanel7.setBounds(650, 210, 400, 200);

        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel10.setText("BBA");

        jLabel11.setText("Recorrido de BBP para implemtacion ");

        jLabel12.setText("BBP");

        textPraBBP.setText(" ");

        jButton1.setText("Obtener");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(textPraBBP, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(textBPA, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(159, 159, 159))
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addComponent(jLabel11)
                    .addContainerGap(283, Short.MAX_VALUE)))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textBPA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textPraBBP, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(80, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel8);
        jPanel8.setBounds(20, 440, 500, 120);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1070, 570);

        setSize(new java.awt.Dimension(1089, 612));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed

        jPanel7.setVisible(true);
        try {
            this.tablaDis.setModel(this.getTableListaDis(true));
        } catch (Exception ex) {
            Logger.getLogger(FrmGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnOkActionPerformed

    private void opcGNDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcGNDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_opcGNDActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        FrmGrafoVista frm = new FrmGrafoVista(gd);
        frm.setSize(400, 400);
        frm.setVisible(true);
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        try {
            this.generarGrafo();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error en grafos", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void tablaDisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaDisMouseClicked
        if (evt.getClickCount() == 2) {
            //Datos a mostrar en pantalla
            //    cargarVista();
        }
    }//GEN-LAST:event_tablaDisMouseClicked

    private void btnOk1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOk1ActionPerformed
        try {
            adyacencias();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error datos incorrectos", "ERROR", JOptionPane.ERROR_MESSAGE);
            this.peso.setText(null);
        }        // TODO add your handling code here:
        jPanel2.setVisible(true);
    //    jPanel8.setVisible(true);
    }//GEN-LAST:event_btnOk1ActionPerformed

    private void btnOk2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOk2ActionPerformed
        jPanel7.setVisible(true);

        try {
            // TODO add your handling code here:
            this.tablaDis.setModel(this.getTableListaDis(false));
        } catch (Exception ex) {

        }
    }//GEN-LAST:event_btnOk2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            String c = "", d = "";

            Integer[] a = gd.toArrayDFS();

        //    Integer[] b = gd.BPA(1);
            System.out.println("sdsadsa");
            System.out.println(Arrays.toString(a));
            //           System.out.println(Arrays.toString(b));

            for (int i = 0; i < a.length - 1; i++) {
                c = c + " " + gd.obtenerEtiqueta(a[i]);

            }
//            for (int i = 0; i < b.length - 1; i++) {
//                d = d + " " + gd.obtenerEtiqueta(b[i]);
//            }
            this.textBPA.setText(c);
            this.textPraBBP.setText(d);
        } catch (Exception ex) {
          //  Logger.getLogger(FrmGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmGrafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmGrafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmGrafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmGrafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmGrafo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup TipoGrafo;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnOk1;
    private javax.swing.JButton btnOk2;
    private javax.swing.JComboBox<String> cbxDestino;
    private javax.swing.JComboBox<String> cbxFin;
    private javax.swing.JComboBox<String> cbxOrigen1;
    private javax.swing.JComboBox<String> cbxinicio;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField nombreVertice;
    private javax.swing.JRadioButton opcGD;
    private javax.swing.JRadioButton opcGND;
    private javax.swing.JTextField peso;
    private javax.swing.JTable tablaDis;
    private javax.swing.JTable tblTabla;
    private javax.swing.JLabel textBPA;
    private javax.swing.JLabel textPraBBP;
    // End of variables declaration//GEN-END:variables
}
